const router = require('express').Router()

const homeRouter = require('./homeRouter')
const userRouter = require('./userRouter')

router.use('/', homeRouter)
router.use('/', userRouter)

module.exports = router