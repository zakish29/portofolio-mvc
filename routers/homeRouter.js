// import module express router
const router = require('express').Router()

// import group router
const homeController = require('../controllers/homeController')

// definisi endpoint terhadap group router
router.get('/', homeController.index)

// export router module
module.exports = router