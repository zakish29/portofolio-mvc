// import module express router
const router = require('express').Router()

const authController = require('../controllers/authController')
// import group router
const userController = require('../controllers/authController')
// const restrict = require('../middlewares/restrict') // restrict local
const restrict = require('../middlewares/restrictJwt')

// definisi endpoint terhadap group router
router.get('/register', userController.register)
router.post('/register', userController.doRegister)

router.get('/login', userController.login)

// router.post('/login', userController.isLogin) // passport local
// router.get('/profile', restrict, userController.profile) // passport local

router.post('/login', userController.doLoginJwt)
router.get('/profile', restrict, authController.profileJwt)

// export router module
module.exports = router