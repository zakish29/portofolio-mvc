require('dotenv').config(); //inisialisasi key
const express = require('express');
const app = express();
const PORT = 3000;
const router= require('./routers');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.set('view engine', 'ejs');

app.use(router);

app.use(express.static("public"));

app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}`));