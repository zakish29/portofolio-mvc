$(function(){
    $("form[name = 'registration']").validate({
        rules: {
            firstname : "required",
            lastname : "required",
            email : {
                required : true,
                email : true,
            },
            password : {
                required: true,
                minlength: 5,
            }
        },
        message: {
            firstname : 'please enter your first name',
            lastname : 'please enter your last name',
            email : {
                required: 'please enter your email',
                email : 'please enter a valid email address',
            },
            password : {
                required : 'please enter your password',
                minlength: 'your password must be atleast 5 characters long'

            }
        },
        submitHandler : function(form){
            form.submit();
        }
    })
})

// AOS
import AOS from 'aos';
import 'aos/dist/aos.css'; // animation on scrollin jquery
// ..
AOS.init();

